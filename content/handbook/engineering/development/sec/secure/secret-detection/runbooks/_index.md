---

title: "Secret Detection Runbooks"
---







## Overview

This page lists runbooks used by the Secret Detection team for monitoring, mitigating and responding to an incident.

## Runbooks

* [Secret Push Protection Monitoring](secret-push-protection-monitoring)
* [Pre-receive secret detection troubleshooting](pre-receive-secret-detection-troubleshooting)
* [Pre-receive secret detection performance testing](pre-receive-secret-detection-performance-testing)
